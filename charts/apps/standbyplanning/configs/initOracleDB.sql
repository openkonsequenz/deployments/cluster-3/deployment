set serveroutput on format word_wrapped;

declare
    in_user VARCHAR2(30) := '&1';
    in_password VARCHAR2(9) := '&2';
    userexist integer;
    sqlStatement varchar2(512);
begin
    select count(*) into userexist from dba_users where username=in_user;
    if (userexist = 0) then
        sqlStatement := 'CREATE USER ' || in_user ||
        ' IDENTIFIED BY ' || in_password ||
        ' DEFAULT TABLESPACE USERS QUOTA UNLIMITED ON USERS' ||
        ' ACCOUNT UNLOCK';
        EXECUTE IMMEDIATE sqlStatement;
        execute immediate 'GRANT CONNECT, RESOURCE TO ' || in_user;
        EXECUTE IMMEDIATE 'GRANT CREATE SESSION, GRANT ANY PRIVILEGE TO ' || in_user;
        EXECUTE IMMEDIATE 'GRANT UNLIMITED TABLESPACE TO ' || in_user;
        dbms_output.put_line('  OK: ' || sqlStatement);
    else
        DBMS_OUTPUT.put_line('  User and Schema already exists: ' || in_user);
    end if;
end;
/